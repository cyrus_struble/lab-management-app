<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::create(array(
            'email' => 'cyrusstruble@gmail.com',
            'name' => 'Cyrus Struble',
            'password' => bcrypt('password'),
            'note' => 'A note goes here',
            'role' => 'superadmin',
        ));

        User::create(array(
            'email' => 'asr020@latech.edu',
            'name' => 'Abhaya Rawal',
            'password' => bcrypt('password'),
            'note' => 'A note goes here',
            'role' => 'student',
        ));

        User::create(array(
            'email' => 'bjs049@latech.edu',
            'name' => 'Brandon Serpas',
            'password' => bcrypt('password'),
            'note' => 'A note goes here',
            'role' => 'admin',
        ));

        User::create(array(
            'email' => 'swk006@latech.edu',
            'name' => 'Skyler King',
            'password' => bcrypt('password'),
            'note' => 'A note goes here',
            'role' => 'superadmin',
        ));

        User::create(array(
            'email' => 'tch031@latech.edu',
            'name' => 'Timothy Hoff',
            'password' => bcrypt('password'),
            'note' => 'A note goes here',
            'role' => 'superadmin',
        ));

        User::create(array(
            'email' => 'naibox@gmail.com',
            'name' => 'Dr. Box',
            'password' => bcrypt('password'),
            'note' => 'A note goes here',
            'role' => 'superadmin',
        ));
    }
}
