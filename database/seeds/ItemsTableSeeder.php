<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\Record;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $items = array(
            array('RLGU36919GX', 'EMHB28756DR', 'macbook'),
            array('ZQYQ94999HE', 'LOJX71312PQ', 'raspberry_pi'),
            array('QTOW07220RU', 'ZRAV40591GR', 'android_phone'),
            array('KHFS18421QV', 'XYHK36411GM', 'ipad'),
            array('XUIA31484HN', 'ILKR76916SU', 'raspberry_pi'),
            array('PEKZ75791ZE', 'OQHU71441XB', 'ipad'),
            array('YXYC69309BG', 'JCDK39273JC', 'android_tablet'),
            array('TOUP36417JB', 'VBBP09040TN', 'macbook'),
            array('HSBO63870EO', 'UBSU51006NF', 'android_phone'),
            array('HGPF57961QI', 'BBDK92523FM', 'macbook'),
            array('ZKBT51236ZJ', 'UOWU26224SF', 'ipad'),
            array('KLGQ13546CL', 'WICQ65898VT', 'macbook'),
            array('TXNA23533EA', 'BGHS19843JQ', 'android_phone'),
            array('UONA25308PI', 'CBZU90054NL', 'android_phone'),
            array('ADTJ35297LU', 'FHGZ51227BN', 'macbook'),
            array('WPSI04210HA', 'HWXW66623MY', 'dell_laptop'),
            array('UMGI98797OG', 'ETVE00964DA', 'dell_laptop'),
            array('DBNB32845RA', 'MIRT98933BG', 'raspberry_pi'),
            array('KANA17887BF', 'ASRN21622AN', 'macbook'),
            array('FMKB59609AF', 'NVSJ89554TT', 'ipad'),
            array('QYQA24090EZ', 'OVIE38381BY', 'android_tablet'),
            array('NVJY86616HE', 'XOWB14667UK', 'android_phone'),
            array('ZNBM04874YD', 'QWER45513DE', 'raspberry_pi'),
            array('GFUL39526HQ', 'WXUJ53376KJ', 'dell_laptop'),
            array('LELP97177DB', 'EERR92209IP', 'iphone'),
            array('GNUR07350GI', 'SMET18822IM', 'macbook'),
            array('VDMD17961SX', 'XCAF19505CQ', 'android_tablet'),
            array('USHL19317LU', 'SFGW94110AV', 'macbook'),
            array('HHWV13260SC', 'OAHA91599MR', 'dell_laptop'),
            array('OTQT72275LO', 'IVSM42356JF', 'android_phone'),
            array('MUTE02022YH', 'HFNB55723OO', 'ipad'),
            array('RFDG38708FH', 'QXTP23304BO', 'raspberry_pi'),
            array('VLQB16869QH', 'FREE14863IA', 'ipad'),
            array('GPKG98940FE', 'XRGV83655HS', 'android_phone'),
            array('KIFL53190VV', 'NAPY58037XN', 'iphone'),
            array('IDZP78149CZ', 'ZLMV05680SO', 'android_phone'),
            array('NUJY02325PV', 'RVEQ55724VH', 'macbook'),
            array('ETCL94867SW', 'RQII97742EU', 'raspberry_pi'),
            array('ZGDD39524JM', 'RXDL73973OO', 'ipad'),
            array('KBUY20223IL', 'SWTZ99943EM', 'raspberry_pi'),
            array('RWCA52611JE', 'KEBY49055CF', 'macbook'),
            array('ZXSR71583FM', 'IHRC50418PI', 'raspberry_pi'),
            array('OBWQ94486PB', 'WPIK20442OR', 'ipad'),
            array('SYKG59790RB', 'FESF05800EO', 'macbook'),
            array('KMIA59643VL', 'MKPM33059LU', 'android_tablet'),
            array('WHYZ69928SG', 'RFSU00725HY', 'iphone'),
            array('YJIU43092SS', 'BWYW05214DK', 'macbook'),
            array('NBXM19932QA', 'LMXZ59707VP', 'macbook'),
            array('GLKU62517VI', 'GJMG76774DE', 'raspberry_pi'),
            array('MWCV51974VL', 'VPAF45365LT', 'iphone'),
            array('HZKA77176ND', 'HAYV91752FF', 'iphone'),
            array('EILV55232GP', 'IZNW95850GH', 'iphone'),
            array('RCUX28924YY', 'FROL63828BI', 'android_phone'),
            array('MRYP98139HF', 'LJGO70903BC', 'dell_laptop'),
            array('PDNH21628RT', 'HISL68494YL', 'macbook'),
            array('LFMW29833UA', 'PGOI77889WX', 'macbook'),
            array('FKJT47448TK', 'HBIT79075VJ', 'raspberry_pi'),
            array('QBMD87551KG', 'HQKD69576YU', 'ipad'),
            array('DBSA02445GB', 'YXLE49011NE', 'android_phone'),
            array('ISQP01760OS', 'LVZC46517OL', 'macbook'),
            array('ALSA96933OW', 'HTWV78388YD', 'dell_laptop'),
            array('IIBQ82534LV', 'SVYY73727YS', 'dell_laptop'),
            array('BQUM66190AL', 'TXJO52001XL', 'raspberry_pi'),
            array('XFOL14987XE', 'PGVC33305TA', 'iphone'),
            );

        foreach ($items as $item) {
            $item = Item::create(array(
                'model_number' => $item[0],
                'serial_number' => $item[1],
                'type' => $item[2],
                'user_id' => null,
            ));

            Record::create(array(
                'item_id' => $item->id,
                'taken_date' => '0000-00-00 00:00:00',
                'return_date' => '0000-00-00 00:00:00',
            ));
        }
    }
}
