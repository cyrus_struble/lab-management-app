<?php

use Illuminate\Database\Seeder;
use App\Record;

class RecordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Record::create(array(
            'item_id' => 1,
            'user_id' => 1,
            'taken_date' => '2015-03-11 11:00:00',
            'return_date' => '2015-03-12 11:00:00',
        ));

        Record::create(array(
            'item_id' => 1,
            'user_id' => 1,
            'taken_date' => '2015-03-13 12:00:00',
            'return_date' => '2015-03-15 12:00:00',
        ));

        Record::create(array(
            'item_id' => 1,
            'user_id' => 1,
            'taken_date' => '2015-04-10 11:00:00',
            'return_date' => '2015-05-16 11:00:00',
        ));
    }
}
