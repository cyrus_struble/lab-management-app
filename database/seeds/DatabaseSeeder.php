<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->command->info('User table seeds finished.');

        $this->call(ItemsTableSeeder::class);
        $this->command->info('Item table seeds finished.');

        // $this->call(RecordsTableSeeder::class);
        // $this->command->info('Record table seeds finished.');

        $this->call(TicketsTableSeeder::class);
        $this->command->info('Ticket table seeds finished.');
    }
}
