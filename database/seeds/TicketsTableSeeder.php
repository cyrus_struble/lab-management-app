<?php

use Illuminate\Database\Seeder;
use App\Ticket;
use App\User;

class TicketsTableSeeder extends Seeder
{
    public function run()
    {
        Ticket::create(array(
            'title' => 'Microsoft IOT Workshop',
            'created_for' => 'naibox@gmail.com',
            'user_id' => User::where('email', '=', 'tch031@latech.edu')->first()->id,
            'description' => 'Can you arrange the lab for 25 seats for the Microsoft IOT training?',
            'state' => 'Resolved',
            'Priority' => 'High',
        ));

        Ticket::create(array(
            'title' => 'Setup OS VMs',
            'created_for' => 'naibox@gmail.com',
            'user_id' => User::where('email', '=', 'cyrusstruble@gmail.com')->first()->id,
            'description' => 'Setup VMs for my operating systems class, see email.',
            'category' => 'VM Setup',
            'state' => 'Pending',
            'Priority' => 'Medium',
        ));
    }
}
