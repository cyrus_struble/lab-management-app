<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned()->nullable();
            $table->foreign('item_id')
                  ->references('id')
                  ->on('items');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
            $table->boolean('checkout_accepted')->default(false);
            $table->boolean('pending_checkout')->default(false);
            $table->boolean('return_accepted')->default(false);
            $table->boolean('pending_return')->default(false);
            $table->string('checkout_authorized_by')->nullable();
            $table->string('return_authorized_by')->nullable();
            $table->timestamp('taken_date')->nullable();
            $table->timestamp('return_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('records');
    }
}
