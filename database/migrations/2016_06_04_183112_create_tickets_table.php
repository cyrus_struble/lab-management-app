<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('notes')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
            $table->string('created_for');
            $table->enum('category', [
                'Miscellaneous',
                'Damaged Item',
                'Lost Item',
                'VM Setup',
                'VM Change',
                'Tour',
                'Training',
            ])->default('Miscellaneous');
            $table->enum('state', [
                'Pending',
                'Resolved',
            ])->default('Pending');
            $table->enum('priority', [
                'Low',
                'Medium',
                'High',
            ]);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tickets');
    }
}
