<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('retired')->default(false);
            $table->string('model_number');
            $table->string('serial_number');
            $table->enum('type', ['unassigned', 'macbook', 'iphone', 'raspberry_pi',
                'dell_laptop', 'android_phone', 'ipad', 'android_tablet', ]);
            $table->boolean('checked_out')->default(false);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('items');
    }
}
