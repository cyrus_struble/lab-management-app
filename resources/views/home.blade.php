@extends('layouts.app')

@section('content')
@if (Session::has('ticketStatus'))
    <div class="alert alert-info text-center">{{ Session::get('ticketStatus') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    <h1>Welcome to the CSRA Lab Management App</h1>
                    <hr>
                    <h3>Looking to check something out?</h3>
                    <p class="lead">Visit the <a href="/inventory">inventory page</a> to see what's available.</p>
                    <h3>Need help, a tour, or a VM setup?</h3>
                    <p class="lead">Create a <a href="create_ticket">troubleshooting ticket</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
