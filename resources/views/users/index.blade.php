@extends('layouts.app')

@section('content')
@if (Session::has('status'))
    <div class="alert alert-info text-center">{{ Session::get('status') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                    <table class="table table-striped table-condensed table-responsive">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Active</th>
                            <th></th>
                        </tr>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role }}</td>
                            @if ($user->active)
                            <td><span class="glyphicon glyphicon-ok"></td>
                            @else
                            <td><span class="glyphicon glyphicon-remove"></td>
                            @endif
                            <td>
                                <form method="GET" action="/users/manage/{{ $user->id }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    @if (Auth::user()->role == 'superadmin')
                                        <button type="submit" class="btn btn-primary">Manage</button>
                                    @else
                                        <button type="" class="btn btn-primary" disabled>Manage</button>
                                    @endif
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
