@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Checkout History for {{ $user->name }}</div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($records as $record)
                                <dl class="dl-horizontal">
                                    <dt>Item Type</dt>
                                    <dd>{{ $record->item->type }}</dd>
                                    <dt>Serial Number</dt>
                                    <dd>{{ $record->item->serial_number }}</dd>
                                    <dt>Model Number</dt>
                                    <dd>{{ $record->item->model_number }}</dd>
                                    <dt>Checked Out</dt>
                                    <dd>{{ $record->taken_date }}</dd>
                                    <dt>Returned</dt>
                                    <dd>{{ $record->return_date }}</dd>
                                </dl>
                                <hr>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
