@extends('layouts.app')

@section('content')
@if (Session::has('userStatus'))
    <div class="alert alert-info text-center">{{ Session::get('userStatus') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Manage User {{ $user->email }}</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $user->name }}</dd>
                        <dt>Email</dt>
                        <dd>{{ $user->email }}</dd>
                        <dt>Role</dt>
                        <dd>{{ $user->role }}</dd>
                        <dt>Created At</dt>
                        <dd>{{ $user->created_at }}</dd>
                        <dt>Updated At</dt>
                        <dd>{{ $user->updated_at }}</dd>
                    </dl>
                    <hr>
                    <div class="col-md-10 col-md-offset-1">
                        <form method="POST" action="/update_user" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="userId" value="{{ $user->id }}">
                            <div class="form-group">
                                @if (count($user->records))
                                    <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#checkoutHistoryModal">View Checkout History</button>
                                    <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#deleteRecentRecordModal">Delete Most Recent Record</button>
                                @else
                                    <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#checkoutHistoryModal" disabled>View Checkout History</button>
                                    <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#deleteRecentRecordModal" disabled>Delete Most Recent Record</button>
                                @endif

                                @if ($user->active == 1)
                                    <button type="submit" name="setActivity" value="inactive" class="btn btn-default btn-block">Set Inactive</button>
                                @else
                                    <button type="submit" name="setActivity" value="active" class="btn btn-default btn-block">Set Active</button>
                                @endif

                                @if ($user->role == 'student')
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="admin">Promote to Admin</button>
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="superadmin">Promote to Super Admin</button>
                                @elseif ($user->role == 'admin')
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="student">Demote to Student</button>
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="superadmin">Promote to Super Admin</button>
                                @elseif ($user->role == 'superadmin' and $user->id == Auth::user()->id)
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="student" disabled>Demote to Student</button>
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="admin" disabled>Demote to Admin</button>
                                @elseif ($user->role == 'superadmin')
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="student">Demote to Student</button>
                                    <button type="submit" name="updateRole" class="btn btn-default btn-block" value="admin">Demote to Admin</button>
                                @endif

                            </div>
                        </form>

                        <!-- Checkout History Modal -->
                        @if (count($user->records))
                        <div class="modal fade" id="checkoutHistoryModal" tabindex="-1" role="dialog" aria-labelledby="checkoutHistoryLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Checkout History for {{ $user->name }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        @foreach ($user->records as $record)
                                            <dl class="dl-horizontal">
                                                <dt>Item Type</dt>
                                                <dd>{{ $record->item->type }}</dd>
                                                <dt>Serial Number</dt>
                                                <dd>{{ $record->item->serial_number }}</dd>
                                                <dt>Model Number</dt>
                                                <dd>{{ $record->item->model_number }}</dd>
                                                <dt>Checked Out</dt>
                                                <dd>{{ $record->taken_date }}</dd>
                                                <dt>Returned</dt>
                                                <dd>{{ $record->return_date }}</dd>
                                            </dl>
                                            <hr>
                                        @endforeach
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <!-- Delete Most Recent History Modal -->
                        @if (count($user->records))
                        <div class="modal fade" id="deleteRecentRecordModal" tabindex="-1" role="dialog" aria-labelledby="deleteRecentRecordLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Delete Most Recent Record</h4>
                                    </div>
                                    <div class="modal-body">
                                        <dl class="dl-horizontal">
                                            <dt>Item Type</dt>
                                            <dd>{{ $user->records->last()->item->type }}</dd>
                                            <dt>Serial Number</dt>
                                            <dd>{{ $user->records->last()->item->serial_number }}</dd>
                                            <dt>Model Number</dt>
                                            <dd>{{ $user->records->last()->item->model_number }}</dd>
                                            <dt>Checked Out</dt>
                                            <dd>{{ $user->records->last()->taken_date }}</dd>
                                            <dt>Returned</dt>
                                            <dd>{{ $user->records->last()->return_date }}</dd>
                                        </dl>
                                    </div>
                                    <div class="modal-footer">
                                        <form method="POST" action="/delete_record" class="form-horizontal">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="recordId" value="{{ $user->records->last()->id }}">
                                            <input type="hidden" name="userId" value="{{ $user->id }}">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
