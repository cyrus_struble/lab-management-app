@extends('layouts.app')
@section('content')
@if (Session::has('ticketStatus'))
<div class="alert alert-info text-center">{{ Session::get('ticketStatus') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Tickets</div>
                <table class="table table-hover table-responsive">
                    <tr>
                        <th>Assigned To</th>
                        <th>Created For</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Priority</th>
                        <th>State</th>
                    </tr>
                    @foreach ($tickets as $ticket)
                    <tr onclick="document.location = '/tickets/{{ $ticket->id }}';">
                        <td>{{ $ticket->user->email }}</td>
                        <td>{{ $ticket->created_for }}</td>
                        <td>{{ $ticket->title }}</td>
                        <td>{{ $ticket->category }}</td>
                        <td>{{ $ticket->priority }}</td>
                        <td>{{ $ticket->state }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection