@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Tickets Assigned to {{ $user->name }}</div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($tickets as $ticket)
                                <dl class="dl-horizontal">
                                    <dt>Title</dt>
                                    <dd>{{ $ticket->title }}</dd>
                                    <dt>Description</dt>
                                    <dd>{{ $ticket->description }}</dd>
                                    <dt>Created for</dt>
                                    <dd>{{ $ticket->created_for }}</dd>
                                    <dt>Category</dt>
                                    <dd>{{ $ticket->category }}</dd>
                                    <dt>State</dt>
                                    <dd>{{ $ticket->state }}</dd>
                                    <dt>Priority</dt>
                                    <dd>{{ $ticket->priority }}</dd>
                                </dl>
                                <form method="POST" action="/update_ticket" class="text-center">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="ticketId" value="{{ $ticket->id }}"
                                    <fieldset class="form-group">
                                        <button type="submit" name="update" value="markResolved" class="btn btn-primary">Mark Resolved</button>
                                    </fieldset>
                                </form>
                                <hr>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection