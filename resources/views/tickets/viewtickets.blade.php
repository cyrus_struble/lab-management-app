@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Tickets Created by {{ $user->name }}</div>
                <div class="panel-body">
                    <div class="row">
                        @foreach ($tickets as $ticket)
                        <dl class="dl-horizontal">
                            <dt>Title</dt>
                            <dd>{{ $ticket->title }}</dd>
                            <dt>Description</dt>
                            <dd>{{ $ticket->description }}</dd>
                            <dt>Category</dt>
                            <dd>{{ $ticket->category }}</dd>
                            <dt>State</dt>
                            <dd>{{ $ticket->state }}</dd>
                            <dt>Priority</dt>
                            <dd>{{ $ticket->priority }}</dd>
                        </dl>
                        <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection