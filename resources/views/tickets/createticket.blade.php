@extends('layouts.app')

@section('content')
@if (Session::has('status'))
    <div class="alert alert-info text-center">{{ Session::get('status') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create Ticket</div>
                <div class="panel-body">
                    <form id="createTicket" method="POST" action="/ticket_submit" class="form-horizontal col-md-10 col-md-offset-1">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="createdFor" value="{{ $createdFor }}">
                        <fieldset class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter title" required>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3" placeholder="Enter problem description" required></textarea>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control" id="category" name="category">
                                <option>Miscellaneous</option>
                                <option>Damaged Item</option>
                                <option>Lost Item</option>
                                <option>VM Setup</option>
                                <option>VM Change</option>
                                <option>Tour</option>
                                <option>Training</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="priority">Priority</label>
                            <select class="form-control" id="priority" name="priority">
                                <option>Low</option>
                                <option>Medium</option>
                                <option>High</option>
                            </select>
                        </fieldset>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit Ticket</button>
                            <input type="button" onclick="location.href='/';" value="Cancel" class="btn btn-danger btn-block"/>
                        </div>
                    </form>
                    <script>
                    $("createTicket").validate();
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
