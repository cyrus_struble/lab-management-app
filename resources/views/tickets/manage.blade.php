@extends('layouts.app')
@section('content')
@if (Session::has('ticketStatus'))
<div class="alert alert-info text-center">{{ Session::get('ticketStatus') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Manage Ticket</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Title</dt>
                        <dd>{{ $ticket->title }}</dd>
                        <dt>Description</dt>
                        <dd>{{ $ticket->description }}</dd>
                        <dt>Assigned To</dt>
                        <dd>{{ $ticket->user->email }}</dd>
                        <dt>Created For</dt>
                        <dd>{{ $ticket->created_for }}</dd>
                        <dt>Category</dt>
                        <dd>{{ $ticket->category }}</dd>
                        <dt>State</dt>
                        <dd>{{ $ticket->state }}</dd>
                        <dt>Priority</dt>
                        <dd>{{ $ticket->priority }}</dd>
                    </dl>
                    <hr>
                    <form method="POST" action="/update_ticket" class="form-horizontal col-md-10 col-md-offset-1">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="ticketId" value="{{ $ticket->id }}">
                        
                        <fieldset class="form-group">
                            @if ($ticket->state == 'Pending')
                                <button type="submit" name="update" value="markResolved" class="btn btn-default btn-block">Mark Resolved</button>
                            @else
                                <button type="submit" name="update" value="markPending" class="btn btn-default btn-block">Mark Pending</button>
                            @endif
                            <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#reassignModal">Reassign</button>
                        </fieldset>
                    </form>

                    <!-- Reassign Modal -->
                    <div class="modal fade" id="reassignModal" tabindex="-1" role="dialog" aria-labelledby="reassignModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="reassignModalLabel">Reassign Ticket</h4>
                                </div>
                                <form method="POST" action="/update_ticket">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="ticketId" value="{{ $ticket->id }}">
                                    <div class="modal-body">
                                        <fieldset class="form-group">
                                            <label for="admins">Assign to</label>
                                            <select class="form-control" id="admins" name="assignTo">
                                                @foreach ($admins as $admin)
                                                    <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                                                @endforeach
                                            </select>
                                        </fieldset>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection