@extends('layouts.app')

@section('content')
@if (Session::has('approvalStatus'))
    <div class="alert alert-info text-center">{{ Session::get('approvalStatus') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Pending Requests</div>
                <div class="panel-body">
                    @foreach ($records as $record)
                        @if ($record->pending_checkout)
                            <div class="row">
                                <dl class="dl-horizontal">
                                    <dt>Checkout Requested By</dt>
                                    <dd>{{ $record->user->email }}</dd>
                                    <dt>Serial Number</dt>
                                    <dd>{{ $record->item->serial_number }}</dd>
                                    <dt>Model Number</dt>
                                    <dd>{{ $record->item->model_number }}</dd>
                                </dl>
                                <form method="POST" action="/manage_request">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="recordId" value="{{ $record->id }}">
                                    <input type="hidden" name="authorizedBy" value="{{ Auth::user()->email }}">
                                    <fieldset class="form-group text-center">
                                        <button type="submit" name="denyCheckout" value="denyCheckout" class="btn btn-danger">Deny Checkout</button>
                                        <button type="submit" name="approveCheckout" value="approveCheckout" class="btn btn-primary">Approve Checkout</button>
                                    </fieldset>
                                </form>
                                <hr>
                            </div>
                        @elseif ($record->pending_return)
                            <div class="row">
                                <dl class="dl-horizontal">
                                    <dt>Return Requested By</dt>
                                    <dd>{{ $record->user->email }}</dd>
                                    <dt>Checkout Authorized By</dt>
                                    <dd>{{ $record->checkout_authorized_by }}</dd>
                                    <dt>Serial Number</dt>
                                    <dd>{{ $record->item->serial_number }}</dd>
                                    <dt>Model Number</dt>
                                    <dd>{{ $record->item->model_number }}</dd>
                                </dl>
                                <form method="POST" action="/manage_request">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="recordId" value=" {{ $record->id }}">
                                    <input type="hidden" name="authorizedBy" value="{{ Auth::user()->email }}">
                                    <fieldset class="form-group text-center">
                                        <button type="submit" name="approveReturn" value="approveReturn" class="btn btn-primary">Approve Return</button>
                                    </fieldset>
                                </form>
                                <hr>
                            </div>
                        @endif
                    @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
