@extends('layouts.app')
@section('content')
@if (Session::has('status'))
<div class="alert alert-info text-center">{{ Session::get('status') }}</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Inventory</div>
                <table class="table table-hover table-responsive">
                    <tr>
                        <th>Type</th>
                        <th>Model Number</th>
                        <th>Serial Number</th>
                        <th>Available</th>
                    </tr>
                    @foreach ($items as $item)
                    <tr onclick="document.location = '/inventory/{{ $item->id }}';">
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->model_number }}</td>
                        <td>{{ $item->serial_number }}</td>
                        <td>
                            @if ($item->checked_out)
                            <span class="glyphicon glyphicon-remove">
                                @else
                                <span class="glyphicon glyphicon-ok">
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection