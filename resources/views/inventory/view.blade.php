@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Inventory - Item Checkout</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                    <dt>Item Type</dt>
                    <dd>{{ $item->type }}</dd>
                    <dt>Serial Number</dt>
                    <dd>{{ $item->serial_number }}</dd>
                    <dt>Model Number</dt>
                    <dd>{{ $item->model_number }}</dd>
                    <dt>Last Checked Out</dt>
                    <dd>{{ $recentTakenRecord->taken_date }}</dd>
                    <dt>Last Returned</dt>
                    <dd>{{ $recentReturnedRecord->return_date }}</dd>
                    <dt>Status</dt>
                        @if ($recentTakenRecord->pending_checkout)
                            <dd>Pending Checkout Approval</dd>
                        @elseif ($recentTakenRecord->pending_return)
                            <dd>Pending Return Approval</dd>
                        @elseif ($recentTakenRecord->return_accepted)
                            <dd>Available</dd>
                        @elseif ($recentTakenRecord->checkout_accepted )
                            <dd>Checked Out</dd>
                        @else
                            <dd></dd>
                        @endif
                    </dl>
                    <hr>
                    <div class="form-group text-center">
                        <form method="POST" action="/inventory/checkout/{{ $item->id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#checkoutHistoryModal">View History</button>
                            @if ($item->checked_out)
                                @if ((Auth::user()->id == $item->user_id) and ($recentTakenRecord->checkout_accepted) and !($recentTakenRecord->pending_return))
                                    <button type="submit" class="btn btn-primary btn-block">Return</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-block" disabled>Return</button>
                                @endif
                            @else
                                <button type="submit" class="btn btn-primary btn-block">Checkout</button>
                            @endif
                            <input type="button" onclick="location.href='/inventory';" value="Cancel" class="btn btn-danger btn-block"/>
                        </form>
                    </div>
                </div>
                <!-- Checkout History Modal -->
                @if (count($records))
                <div class="modal fade" id="checkoutHistoryModal" tabindex="-1" role="dialog" aria-labelledby="checkoutHistoryLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Checkout History</h4>
                            </div>
                            <div class="modal-body">
                                @foreach ($records as $record)
                                    <dl class="dl-horizontal">
                                        <dt>Checked Out By</dt>
                                        @if ($record->user)
                                            <dd>{{ $record->user->email }}</dd>
                                        @else
                                            <dd></dd>
                                        @endif
                                        <dt>Serial Number</dt>
                                        <dd>{{ $record->item->serial_number }}</dd>
                                        <dt>Model Number</dt>
                                        <dd>{{ $record->item->model_number }}</dd>
                                        <dt>Checked Out</dt>
                                        <dd>{{ $record->taken_date }}</dd>
                                        <dt>Returned</dt>
                                        <dd>{{ $record->return_date }}</dd>
                                    </dl>
                                    <hr>
                                @endforeach
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
