<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Item extends Model
{
    protected $fillable = [
        'model_number',
        'serial_number',
        'type',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function records()
    {
        return $this->hasMany('App\Record');
    }
}
