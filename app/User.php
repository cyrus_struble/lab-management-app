<?php

namespace app;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function records()
    {
        return $this->hasMany('App\Record');
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function ticketsCount()
    {
        return $this->hasOne('App\Ticket')
                    ->selectRaw('user_id, count(*) as aggregate')
                    ->groupBy('user_id');
    }

    public function getTicketsCountAttribute()
    {
        if ($this->relationLoaded('ticketsCount')) {
            $this->load('ticketsCount');
        }

        $related = $this->getRelation('ticketsCount');

        return ($related) ? (int) $related->aggregate : 0;
    }
}
