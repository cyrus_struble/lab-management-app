<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', 'HomeController@index');

// Inventory Routes
Route::get('/inventory', 'InventoryController@index');
Route::get('/inventory/{item}', 'InventoryController@viewItem');
Route::get('/view_requests', 'InventoryController@viewRequests');
Route::post('/inventory/checkout/{item}', 'InventoryController@checkoutItem');
Route::post('/manage_request', 'InventoryController@manageRequest');

// Record Routes
Route::post('/delete_record', 'RecordController@delete');

// User Routes
Route::get('/users', 'UserController@index');
Route::get('/users/manage/{user}', 'UserController@manage');
Route::get('/checkout_history', 'UserController@showCheckoutHistory');
Route::post('/update_user', 'UserController@update');

// Ticket Routes
Route::get('/tickets', 'TicketController@index');
Route::get('/tickets/{ticket}', 'TicketController@manage');
Route::get('/assigned_tickets', 'TicketController@viewAssignedTickets');
Route::get('/my_tickets', 'TicketController@viewMyTickets');
Route::get('/create_ticket', 'TicketController@showTicketForm');
Route::post('/ticket_submit', 'TicketController@create');
Route::post('/update_ticket', 'TicketController@update');
