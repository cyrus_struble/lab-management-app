<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Item;
use App\Record;
use Auth;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('inventory.index', ['items' => Item::all()]);
    }

    public function viewItem(Item $item)
    {
        $recentTakenRecord = Record::where('item_id', $item->id)->recentTaken();
        $recentReturnedRecord = Record::where('item_id', $item->id)->recentReturned();
        $records = Record::where('item_id', '=', $item->id)->with('user')->get();

        return view('inventory.view', [
            'item' => $item,
            'recentTakenRecord' => $recentTakenRecord,
            'recentReturnedRecord' => $recentReturnedRecord,
            'records' => $records,
        ]);
    }

    public function viewRequests(Request $request)
    {
        if (Auth::user()->role == 'admin' or Auth::user()->role == 'superadmin') {
            $records = Record::where('pending_checkout', '=', '1')
                ->orWhere('pending_return', '=', '1')->with('item', 'user')->get();

            return view('inventory.requests', ['records' => $records]);
        } else {
            return view('home');
        }
    }

    public function manageRequest(Request $request)
    {
        $record = Record::find($request->input('recordId'));

        $timestamp = date('Y-m-d G:i:s');

        if ($request->input('denyCheckout')) {
            $item = $record->item;
            $item->checked_out = 0;
            $item->save();

            $record->delete();
            $request->session()->flash('approvalStatus', 'Checkout denied. Record deleted.');
        } elseif ($request->input('approveCheckout')) {
            $record->checkout_accepted = 1;
            $record->pending_checkout = 0;
            $record->checkout_authorized_by = $request->input('authorizedBy');
            $record->taken_date = $timestamp;
            $record->save();
            $request->session()->flash('approvalStatus', 'Checkout approved.');
        } elseif ($request->input('approveReturn')) {
            $item = $record->item;
            $item->checked_out = 0;
            $item->save();

            $record->return_accepted = 1;
            $record->pending_return = 0;
            $record->return_authorized_by = $request->input('authorizedBy');
            $record->return_date = $timestamp;
            $record->save();
            $request->session()->flash('approvalStatus', 'Return approved.');
        }

        $records = Record::where('pending_checkout', '=', '1')
                ->orWhere('pending_return', '=', '1')->with('item', 'user')->get();

        return view('inventory.requests', ['records' => $records]);
    }

    public function checkoutItem(Request $request, Item $item)
    {
        // Retrieve the current user
        $user = Auth::user();

        $timestamp = date('Y-m-d G:i:s');

        if ($item->checked_out) {
            $record = Record::where('item_id', $item->id)->recentTaken();

            // Admins do not need approval for returns, but others do
            if ($user->role == 'admin' or $user->role == 'superadmin') {
                $item->checked_out = false;
                $item->save();

                $record->user()->associate($user);
                $record->item()->associate($item);
                $record->return_accepted = 1;
                $record->return_authorized_by = $user->email;
                $record->return_date = $timestamp;
                $record->save();

                $request->session()->flash('status', 'Success! Item was returned.');
            } else {
                $record->pending_return = true;
                $record->save();

                $request->session()->flash('status', 'Item return pending, see admin to return your item.');
            }
        } else {
            $item->checked_out = true;
            $item->user()->associate($user);
            $item->save();

            // Admins do not need approval for checkout, but others do
            if ($user->role == 'admin' or $user->role == 'superadmin') {
                Record::create(array(
                    'item_id' => $item->id,
                    'user_id' => $user->id,
                    'checkout_accepted' => true,
                    'checkout_authorized_by' => $user->email,
                    'taken_date' => $timestamp,
                    'return_date' => '0000-00-00 00:00:00',
                ));
                $request->session()->flash('status', 'Success! Item checked out.');
            } else {
                Record::create(array(
                    'item_id' => $item->id,
                    'user_id' => $user->id,
                    'pending_checkout' => true,
                    'taken_date' => $timestamp,
                    'return_date' => '0000-00-00 00:00:00',
                ));
                $request->session()->flash('status', 'Item checkout pending, see an admin to retrieve your item.');
            }
        }

        return view('inventory.index', [
            'items' => Item::all(), ]);
    }
}
