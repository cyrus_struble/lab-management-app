<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Record;
use App\Item;
use App\User;

class RecordController extends Controller
{
    public function delete(Request $request)
    {
        $record = Record::find($request->input('recordId'));

        // Set the corresponding item's checked_out to false
        $item = Item::find($record->item_id);
        $item->checked_out = 0;
        $item->save();

        if ($record->delete()) {
            $request->session()->flash('status', 'Record sucessfully deleted.');
        } else {
            $request->session()->flash('status', 'Error deleting record.');
        }

        $user = User::with('records')->where('id', '=', $request->input('userId'))->get()->first();

        return view('users.manage', [
                'user' => $user,
        ]);
    }
}
