<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->role == 'admin' or $user->role == 'superadmin') {
            return view('users.index', ['users' => User::all()]);
        } else {
            return view('home');
        }
    }

    public function showCheckoutHistory()
    {
        $user = Auth::user();

        return view('users.checkouthistory', [
            'user' => $user,
            'records' => $user->records,
        ]);
    }

    public function manage(Request $request, User $user)
    {
        $authenticatedUser = Auth::user();

        if ($authenticatedUser->role == 'superadmin') {
            return view('users.manage', [
                'authenticatedUser' => $authenticatedUser,
                'user' => $user,
            ]);
        } else {
            $request->session()->flash('userStatus', 'Only super admins may manage users!');

            return view('users.index', ['users' => User::all()]);
        }
    }

    public function update(Request $request)
    {
        $user = User::find($request->input('userId'));

        if ($request->input('setActivity')) {
            if ($request->input('setActivity') == 'inactive') {
                $user->active = 0;
            } else {
                $user->active = 1;
            }

            $user->save();
            $request->session()->flash('userStatus', 'User activity updated.');
        } elseif ($request->input('updateRole')) {
            $user->role = $request->input('updateRole');
            $user->save();
            $request->session()->flash('userStatus', 'User role updated.');
        } elseif ($request->input('note')) {
            $user->note = htmlspecialchars($request->input('note'));
            $user->save();
            $request->session()->flash('userStatus', 'User note updated');
        }

        return view('users.manage', [
                'user' => $user,
        ]);
    }
}
