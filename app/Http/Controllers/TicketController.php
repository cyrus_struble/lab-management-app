<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Ticket;
use Auth;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();

        if ($user->role == 'superadmin') {
            return view('tickets.index', [
                'tickets' => Ticket::all(),
            ]);
        } else {
            return view('home');
        }
    }

    public function viewAssignedTickets(Request $request)
    {
        $user = Auth::user();

        if ($user->role == 'admin' or $user->role == 'superadmin') {
            $tickets = Ticket::where('user_id', '=', $user->id)->where('state', '=', 'Pending')->get();

            return view('tickets.assignedtickets', [
                'user' => $user,
                'tickets' => $tickets,
            ]);
        } else {
            return view('home');
        }
    }

    public function viewMyTickets(Request $request)
    {
        $user = Auth::user();

        $tickets = Ticket::where('created_for', '=', $user->email)->get();

        return view('tickets.viewtickets', [
            'user' => $user,
            'tickets' => $tickets,
        ]);
    }

    public function showTicketForm()
    {
        $createdFor = Auth::user()->id;

        return view('tickets.createticket', ['createdFor' => $createdFor]);
    }

    public function manage(Ticket $ticket, Request $request)
    {
        $admins = User::where('role', '=', 'admin')->orWhere('role', '=', 'superadmin')->get();

        return view('tickets.manage', [
            'ticket' => $ticket,
            'admins' => $admins,
        ]);
    }

    public function create(Request $request)
    {
        // Determine admin to assign ticket to
        $users = User::with('ticketsCount')->get();

        // Filter out non-admin and non-superadmin users
        $filtered = $users->filter(function ($item) {
            return $item->role == 'admin' or $item->role == 'superadmin';
        });

        // Sort according to fewest tickets first
        $sorted = $filtered->sortBy('ticketsCount');

        // Assign ticket to admin with fewest tickets
        $assignedAdmin = $sorted->first();

        Ticket::create([
            'user_id' => $assignedAdmin->id,
            'created_for' => Auth::user()->email,
            'title' => htmlspecialchars($request->input('title')),
            'description' => htmlspecialchars($request->input('description')),
            'category' => $request->input('category'),
            'priority' => $request->input('priority'),
        ]);

        $request->session()->flash('ticketStatus', 'Ticket created. We\'ll get back to you shortly.');

        return view('home');
    }

    public function update(Request $request)
    {
        $ticket = Ticket::find($request->input('ticketId'));

        if ($request->input('update') == 'markResolved') {
            $ticket->state = 'Resolved';
            $ticket->save();
        } elseif ($request->input('update') == 'markPending') {
            $ticket->state = 'Pending';
            $ticket->save();
        } elseif ($request->input('assignTo')) {
            $ticket->user_id = $request->input('assignTo');
            $ticket->save();
        }

        $user = Auth::user();
        $tickets = Ticket::where('user_id', '=', $user->id)->where('state', '=', 'Pending')->get();

        return redirect()->back()->with([
            'user' => $user,
            'tickets' => $tickets,
        ]);
    }
}
