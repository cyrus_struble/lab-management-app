<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Item;
use App\User;

class Record extends Model
{
    use SoftDeletes;

    protected $fillable = ['taken_date',
        'return_date',
        'user_id',
        'item_id',
        'checkout_accepted',
        'return_accepted',
        'checkout_authorized_by',
        'return_authorized_by,',
        'pending_checkout',
        'pending_return',
    ];

    protected $dates = ['deleted_at'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeRecentTaken($query)
    {
        return $query->orderBy('taken_date', 'desc')->take(1)->get()->first();
    }

    public function scopeRecentReturned($query)
    {
        return $query->orderBy('return_date', 'desc')->get()->first();
    }
}
