<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;
use App\Item;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Item::saving(function ($model) {
            foreach ($model->toArray() as $name => $value) {
                if (empty($value)) {
                    $model->{$name} = null;
                }
            }

            return true;
        });
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}
