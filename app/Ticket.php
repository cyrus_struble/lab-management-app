<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Ticket extends Model
{
    protected $fillable = [
        'title',
        'description',
        'notes',
        'user_id',
        'created_for',
        'category',
        'priority',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
